/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.5.40 : Database - liuyou
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`liuyou` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `liuyou`;

/*Table structure for table `allusers` */

DROP TABLE IF EXISTS `allusers`;

CREATE TABLE `allusers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `cx` varchar(50) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `allusers` */

insert  into `allusers`(`ID`,`username`,`pwd`,`cx`,`addtime`) values 
(1,'cd','cd','超级管理员','2020-03-01 02:04:24');

/*Table structure for table `dx` */

DROP TABLE IF EXISTS `dx`;

CREATE TABLE `dx` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `leibie` varchar(50) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `dx` */

insert  into `dx`(`ID`,`leibie`,`content`) values 
(1,'系统公告','<P>&nbsp;&nbsp;&nbsp; 欢迎大家登陆我站，我站主要是为广大朋友精心制作的一个系统，希望大家能够在我站获得一个好心情，谢谢！</P>'),
(2,'系统简介','塞北村镇旅游网站是一种时尚的旅游方式,即以“张扬个性、亲近自然、放松身心”为目标,完全自主地选择和安排旅游活动,且没有全程导游陪同的一种旅游方式。自助旅游最大的优点是游客的自主性强,可以随心所欲去选择自己的路线、时间和地方,也可以自主地支配自己的开支,是较为成熟的旅游者所乐意采取的旅游方式。 ');

/*Table structure for table `gwc` */

DROP TABLE IF EXISTS `gwc`;

CREATE TABLE `gwc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `bianhao` varchar(50) DEFAULT NULL,
  `mingcheng` varchar(50) DEFAULT NULL,
  `tupian` varchar(50) DEFAULT NULL,
  `jiage` double DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `shuliang` int(11) DEFAULT NULL,
  `isfk` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `gwc` */

insert  into `gwc`(`ID`,`username`,`bianhao`,`mingcheng`,`tupian`,`jiage`,`addtime`,`shuliang`,`isfk`) values 
(1,'555','004','勋章','upload/1334986363312.jpg',420,'2020-03-01 02:04:24',2,'否'),
(2,'yyy','003','金龙油','upload/1334986338171.jpg',13,'2020-03-01 02:04:24',3,'否');

/*Table structure for table `jingdianxinxi` */

DROP TABLE IF EXISTS `jingdianxinxi`;

CREATE TABLE `jingdianxinxi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `mingcheng` varchar(50) DEFAULT NULL,
  `xingji` varchar(50) DEFAULT NULL,
  `dizhi` varchar(50) DEFAULT NULL,
  `dianhua` varchar(50) DEFAULT NULL,
  `jianjie` varchar(500) DEFAULT NULL,
  `zhaopian` varchar(50) DEFAULT NULL,
  `piaojia` varchar(50) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `jingdianxinxi` */

insert  into `jingdianxinxi`(`ID`,`mingcheng`,`xingji`,`dizhi`,`dianhua`,`jianjie`,`zhaopian`,`piaojia`,`addtime`) values 
(1,'塞北村镇','三星级','塞北','3463636','erreherhre','upload/1335007505203.jpg','50','2020-03-01 02:04:24'),
(2,'塞北村镇','四星级','gregerhreh','3466346','wgewhwehew','upload/1335007537000.jpg','235','2020-03-01 02:04:24'),
(3,'塞北村镇2','五星级','塞北','52363626','wgewegdsgsd','upload/1335007591843.jpg','2553','2020-03-01 02:04:24'),
(4,'塞北村镇','五星级','塞北','52632632','whewgwegwe','upload/1335007669640.jpg','52','2020-03-01 02:04:24'),
(5,'fgjgfjfg','三星级','454745','754754','trjrtjrt','upload/1335010069953.jpg','23','2020-03-01 02:04:24');

/*Table structure for table `jiudianxinxi` */

DROP TABLE IF EXISTS `jiudianxinxi`;

CREATE TABLE `jiudianxinxi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `bianhao` varchar(50) DEFAULT NULL,
  `mingcheng` varchar(300) DEFAULT NULL,
  `xingji` varchar(50) DEFAULT NULL,
  `dizhi` varchar(300) DEFAULT NULL,
  `dianhua` varchar(50) DEFAULT NULL,
  `zhaopian` varchar(50) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `jiudianxinxi` */

insert  into `jiudianxinxi`(`ID`,`bianhao`,`mingcheng`,`xingji`,`dizhi`,`dianhua`,`zhaopian`,`beizhu`,`addtime`) values 
(1,'001','塞北村镇大酒店','4','sdgdsgsd','526236','upload/1333670057515.jpg','gewgwe','2020-03-01 02:04:24'),
(2,'002','塞北诚大饭店','5','hthjtrjrt','3646363','upload/1333670098265.jpg','dgdsgsda','2020-03-01 02:04:24'),
(3,'003','塞北国际酒店','5','reherher','543564363','upload/1333670126718.jpg','rrtggg','2020-03-01 02:04:24'),
(4,'004','塞北新同酒店','2','sdgsdgsd','23523624','upload/1333674587937.jpg','fwegewgew','2020-03-01 02:04:24'),
(5,'006','hhethe','3','herher','6437657','upload/1333675906468.jpg','herher','2020-03-01 02:04:24'),
(6,'hreher','jrejre','4','erhreher','54354364','upload/1334513051859.jpg','gewgew','2020-03-01 02:04:24');

/*Table structure for table `jiudianyuding` */

DROP TABLE IF EXISTS `jiudianyuding`;

CREATE TABLE `jiudianyuding` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `jiudianmingcheng` varchar(300) DEFAULT NULL,
  `kefangbianhao` varchar(50) DEFAULT NULL,
  `jiage` varchar(50) DEFAULT NULL,
  `yudingshijian` varchar(50) DEFAULT NULL,
  `yudingtianshu` varchar(50) DEFAULT NULL,
  `nindexingming` varchar(50) DEFAULT NULL,
  `nindedianhua` varchar(50) DEFAULT NULL,
  `nindeshenfenzheng` varchar(300) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  `yonghuming` varchar(50) DEFAULT NULL,
  `issh` varchar(2) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `jiudianyuding` */

insert  into `jiudianyuding`(`ID`,`jiudianmingcheng`,`kefangbianhao`,`jiage`,`yudingshijian`,`yudingtianshu`,`nindexingming`,`nindedianhua`,`nindeshenfenzheng`,`beizhu`,`yonghuming`,`issh`,`addtime`) values 
(1,'塞北国际酒店','102','128','2012-04-10','3','wefew','235325','523532523','gwegw','555','是','2020-03-01 02:04:24'),
(2,'塞北国际酒店','101','85','2012-04-11','3','eger','3463463','634634634','erher','666','是','2020-03-01 02:04:24'),
(3,'塞北国际酒店','405','2355','2012-04-14','2','erger','4363463','64363','herhe','777','是','2020-03-01 02:04:24'),
(4,'塞北国际酒店','102','128','2012-04-17','2','fewe','623623','23623632','6223','bbb','是','2020-03-01 02:04:24'),
(5,'塞北国际酒店','405','2355','2012-04-25','3','greger','3463634','6346346','43erher','yyy','是','2020-03-01 02:04:24');

/*Table structure for table `kefang` */

DROP TABLE IF EXISTS `kefang`;

CREATE TABLE `kefang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `jiudian` varchar(300) DEFAULT NULL,
  `kefangbianhao` varchar(50) DEFAULT NULL,
  `leixing` varchar(50) DEFAULT NULL,
  `daxiao` varchar(50) DEFAULT NULL,
  `jiage` varchar(50) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `kefang` */

insert  into `kefang`(`ID`,`jiudian`,`kefangbianhao`,`leixing`,`daxiao`,`jiage`,`beizhu`,`addtime`) values 
(1,'塞北国际酒店','101','经济型','单人间','85','sgdgs','2020-03-01 02:04:24'),
(2,'塞北国际酒店','102','商务型','双人间','128','gdasgad','2020-03-01 02:04:24'),
(3,'塞北国际酒店','205','商务型','多人间','400','sgsdgs','2020-03-01 02:04:24'),
(4,'温州国际酒店','405','商务型','双人间','2355','egwgewg','2020-03-01 02:04:24'),
(5,'hhethe','402','商务型','双人间','446','herhre','2020-03-01 02:04:24'),
(6,'jrejre','6645','舒适型','双人间','333','rre','2020-03-01 02:04:24');

/*Table structure for table `liuyanban` */

DROP TABLE IF EXISTS `liuyanban`;

CREATE TABLE `liuyanban` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cheng` varchar(50) DEFAULT NULL,
  `xingbie` varchar(2) DEFAULT NULL,
  `QQ` varchar(50) DEFAULT NULL,
  `youxiang` varchar(50) DEFAULT NULL,
  `dianhua` varchar(50) DEFAULT NULL,
  `neirong` varchar(500) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `huifuneirong` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `liuyanban` */

insert  into `liuyanban`(`ID`,`cheng`,`xingbie`,`QQ`,`youxiang`,`dianhua`,`neirong`,`addtime`,`huifuneirong`) values 
(1,'erhre','1','2352362','wegwe@163.com','23262362','gwegewg','2020-03-01 02:04:24','gdfgd'),
(2,'hfdshdsf','2','3463473','erhe@163.com','3463634','ggwgwegw','2020-03-01 02:04:24','gfdgdfgdf'),
(3,'jrttrktr','4','634633','geer@163.com','34633634','gehre','2020-03-01 02:04:24','tjtrjrt'),
(4,'rrtr','1','34643634','4e@163.com','6436435','gegew','2020-03-01 02:04:24','hreher'),
(5,'tktykty','3','5754745','jr@163.com','54363463','erher','2020-03-01 02:04:24','sdgsdgs');

/*Table structure for table `pinglun` */

DROP TABLE IF EXISTS `pinglun`;

CREATE TABLE `pinglun` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `xinwenID` varchar(50) DEFAULT NULL,
  `pinglunneirong` varchar(500) DEFAULT NULL,
  `pinglunren` varchar(50) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `pinglun` */

insert  into `pinglun`(`ID`,`xinwenID`,`pinglunneirong`,`pinglunren`,`addtime`) values 
(1,'2','hreher','555','2020-03-01 02:04:24'),
(2,'2','hehre','yyy','2020-03-01 02:04:24'),
(3,'4','2222','cd','2020-03-01 02:04:24'),
(4,'7','333','cd','2020-03-01 02:04:24');

/*Table structure for table `xianlujiatuan` */

DROP TABLE IF EXISTS `xianlujiatuan`;

CREATE TABLE `xianlujiatuan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `xianlubianhao` varchar(50) DEFAULT NULL,
  `xianlumingcheng` varchar(300) DEFAULT NULL,
  `chufadi` varchar(50) DEFAULT NULL,
  `mudedi` varchar(50) DEFAULT NULL,
  `jiaotonggongju` varchar(50) DEFAULT NULL,
  `feiyong` varchar(50) DEFAULT NULL,
  `chuxingshijian` varchar(50) DEFAULT NULL,
  `faburen` varchar(50) DEFAULT NULL,
  `canjiaren` varchar(50) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  `issh` varchar(2) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `xianlujiatuan` */

insert  into `xianlujiatuan`(`ID`,`xianlubianhao`,`xianlumingcheng`,`chufadi`,`mudedi`,`jiaotonggongju`,`feiyong`,`chuxingshijian`,`faburen`,`canjiaren`,`beizhu`,`issh`,`addtime`) values 
(1,'002','塞北两日游','塞北','塞北','自行车','480','2012-04-09','hsg','555','xxx','否','2020-03-01 02:04:24'),
(2,'002','塞北山两日游','塞北','塞北','自行车','480','2012-04-09','hsg','yyy','tkkty','否','2020-03-01 02:04:24');

/*Table structure for table `xinwentongzhi` */

DROP TABLE IF EXISTS `xinwentongzhi`;

CREATE TABLE `xinwentongzhi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `biaoti` varchar(300) DEFAULT NULL,
  `leibie` varchar(50) DEFAULT NULL,
  `neirong` longtext,
  `tianjiaren` varchar(50) DEFAULT NULL,
  `shouyetupian` varchar(50) DEFAULT NULL,
  `dianjilv` varchar(10) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `xinwentongzhi` */

insert  into `xinwentongzhi`(`ID`,`biaoti`,`leibie`,`neirong`,`tianjiaren`,`shouyetupian`,`dianjilv`,`addtime`) values 
(1,'法国阿尔萨斯旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd','upload/1317085936937.jpg','2','2020-03-01 02:04:24'),
(2,'美国旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd','upload/1317085936937.jpg','6','2020-03-01 02:04:24'),
(3,'日本旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd','upload/1317085936937.jpg','4','2020-03-01 02:04:24'),
(4,'英国旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd','upload/1317085936937.jpg','7','2020-03-01 02:04:24'),
(5,'澳大利亚旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'7','2020-03-01 02:04:24'),
(6,'瑞典旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'38','2020-03-01 02:04:24'),
(7,'西班牙中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'9','2020-03-01 02:04:24'),
(8,'韩国旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'3','2020-03-01 02:04:24'),
(9,'阿拉斯加旅行中的美味艳遇','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'3','2020-03-01 02:04:24'),
(10,'新冠防护须知3','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'4','2020-03-01 02:04:24'),
(11,'新冠防护须知2','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'3','2020-03-01 02:04:24'),
(12,'新冠防护须知1','news','阿尔萨斯人有一种不同于法国其他地方的气质，淡薄、质朴中带着几分豪爽，他们住着古老的木筋房，大块吃肉、大口喝酒，隐居乡村。开辆法国车，顺着阿尔萨斯乡间小路一头扎进去，便能很容易体味到只属于那里的美食、美酒和生活态度。','cd',NULL,'7','2020-03-01 02:04:24');

/*Table structure for table `yonghuzhuce` */

DROP TABLE IF EXISTS `yonghuzhuce`;

CREATE TABLE `yonghuzhuce` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `yonghuming` varchar(50) DEFAULT NULL,
  `mima` varchar(50) DEFAULT NULL,
  `xingming` varchar(50) DEFAULT NULL,
  `xingbie` varchar(2) DEFAULT NULL,
  `chushengnianyue` varchar(50) DEFAULT NULL,
  `QQ` varchar(50) DEFAULT NULL,
  `youxiang` varchar(50) DEFAULT NULL,
  `dianhua` varchar(50) DEFAULT NULL,
  `shenfenzheng` varchar(50) DEFAULT NULL,
  `touxiang` varchar(50) DEFAULT NULL,
  `dizhi` varchar(300) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `issh` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `yonghuzhuce` */

insert  into `yonghuzhuce`(`ID`,`yonghuming`,`mima`,`xingming`,`xingbie`,`chushengnianyue`,`QQ`,`youxiang`,`dianhua`,`shenfenzheng`,`touxiang`,`dizhi`,`beizhu`,`addtime`,`issh`) values 
(1,'555','555','333','男','2012-04-02','5252532','hehe@163.com','13186835580','363463634','upload/1333961148718.jpg','龙港龙翔路1170号','sdgsdg','2020-03-01 02:04:24','是'),
(2,'leejie','leejie','李龙','男','2012-04-03','54326436','sdgss@163.com','644875874','330327187945','upload/1333963398718.gif','温州飞翔路2号','ggwgwe','2020-03-01 02:04:24','是'),
(3,'mygod','mygod','陈德才','男','2012-04-02','54363346','fewg@163.com','52346462','634643636','upload/1333963445546.gif','杭州四季青广场3号','gregreh','2020-03-01 02:04:24','是'),
(4,'xwxxmx','xwxxmx','吴江','男','2012-04-04','6436363','geer@163.com','23532632','6347347434','upload/1333963661093.jpg','上海浦东区22号','erherhre','2020-03-01 02:04:24','是'),
(5,'yyy','yyy','yyy','男','2012-04-10','765765765','jjt@163.com','526234643','364334634','upload/1335009847625.gif','rherh','reher','2020-03-01 02:04:24','是'),
(6,'admin01','123456','admin01','男','2005-05-03','2112312213','','','','','','','2018-05-09 19:38:40','是');

/*Table structure for table `zijiayouxianlu` */

DROP TABLE IF EXISTS `zijiayouxianlu`;

CREATE TABLE `zijiayouxianlu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `xianlubianhao` varchar(50) DEFAULT NULL,
  `xianlumingcheng` varchar(300) DEFAULT NULL,
  `chufadi` varchar(50) DEFAULT NULL,
  `mudedi` varchar(50) DEFAULT NULL,
  `jiaotonggongju` varchar(50) DEFAULT NULL,
  `feiyong` varchar(50) DEFAULT NULL,
  `chuxingshijian` varchar(50) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  `faburen` varchar(50) DEFAULT NULL,
  `addtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `zijiayouxianlu` */

insert  into `zijiayouxianlu`(`ID`,`xianlubianhao`,`xianlumingcheng`,`chufadi`,`mudedi`,`jiaotonggongju`,`feiyong`,`chuxingshijian`,`beizhu`,`faburen`,`addtime`) values 
(1,'001','塞北三日游','塞北','塞北湖','汽车','880','2020-05-26','gewgwe','hsg','2020-03-01 02:04:24'),
(2,'002','塞北两日游','塞北','塞北山','自行车','480','2020-05-26','gewgewg','hsg','2020-03-01 02:04:24'),
(3,'003','hgreherh','erher','herjer','轮船','444','2020-05-26','rehrehre','hsg','2020-03-01 02:04:24'),
(4,'tyu56','56i65i','i65i65','i65i65','火车','44','2020-05-26','tuytu','555','2020-03-01 02:04:24'),
(5,'trhrt','jrtjrt','jtr','ktrktr','火车','350','2020-05-26','eryreye','bbb','2020-03-01 02:04:24'),
(6,'ktykty','ktyktyl','tlty','tkytkyt','飞机','444','2020-05-26','rthhrt','yyy','2020-03-01 02:04:24'),
(7,'hrher','erjejer','jrejr','ejerje','汽车','22','2020-05-26','gehreher','555','2020-03-01 02:04:24');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
